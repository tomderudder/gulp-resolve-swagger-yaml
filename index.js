'use strict'

// modules
const { resolveRefsAt } = require('json-refs')
const { safeLoad, safeDump } = require('js-yaml')
const through = require('through2')
const gutil = require('gulp-util')

// constants
const PLUGIN_NAME = 'gulp-resolve-swagger-yaml'
const PluginError = gutil.PluginError
const SUPPORTED_FORMATS = ['yaml', 'json']
const resolveOptions = {
  filter: ['relative', 'remote'],
  loaderOptions: {
    processContent: (res, callback) => callback(null, safeLoad(res.text))
  }
}

/**
 * @function
 * @name resolveYaml
 */
module.exports = function({ formats = [] }) {
  return through.obj(function(file, enc, callback) {
    if (file !== null && file.isBuffer()) {
      // isBuffer
      resolveRefsAt(file.path, resolveOptions)
        // resolve success
        .then(({ resolved }) => {
          formats
            .filter(format => SUPPORTED_FORMATS.includes(format))
            .map(format => {
              const newFile = new gutil.File()
              let data

              if (format === 'json') data = JSON.stringify(resolved, null, 2)
              if (format === 'yaml') data = safeDump(resolved)
              newFile.path = 'swagger.' + format
              newFile.contents = Buffer.from(data)
              this.push(newFile)
            })
          callback()
        })

        // resolve failed
        .catch(err => {
          this.emit('error', new PluginError(PLUGIN_NAME, err.message))
          callback()
        })
    } else {
      this.emit(
        'error',
        new PluginError(PLUGIN_NAME, 'Only Buffer format is supported')
      )
      callback()
    }
  })
}
